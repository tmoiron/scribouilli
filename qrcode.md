---
title: "QRCode"
order: 3
in_menu: true
---
<article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/LibreQR.png">
    </div>
    <div>
      <h2>LibreQR</h2>
      <p>Générateur de codes QR sur le Web.</p>
      <div>
        <a href="https://framalibre.org/notices/libreqr.html">Vers la notice Framalibre</a>
        <a href="https://code.antopie.org/miraty/libreqr">Vers le site</a>
      </div>
    </div>
  </article> 